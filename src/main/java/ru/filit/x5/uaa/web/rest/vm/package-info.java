/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.filit.x5.uaa.web.rest.vm;
